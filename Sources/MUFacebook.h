//
//  MUFacebook.h
//  APITest
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MUFacebookUser.h"

// key for create invite dictionary
#define kInviteTitleKey         @"title"
#define kInviteMessageKey       @"message"
#define kInviteToKey            @"to"

// key for create me feed dictionary
#define kFeedMessageKey         @"message"      // or link
#define kFeedLinkKey            @"link"         // ro message
#define kFeedNameKey            @"name"         //can only be used if kFeedLinkKey is specified
#define kFeedCaptionKey         @"caption"      //can only be used if kFeedLinkKey is specified
#define kFeedDescriptionKey     @"description"  //can only be used if kFeedLinkKey is specified
#define kFeedPictureURLKey      @"picture"      //can only be used if kFeedLinkKey is specified


typedef void (^MUFacebookCallback)(id data, NSError* error, BOOL canceled);

@interface MUFacebook : NSObject
{
    NSArray* permissions;
    NSString* profileParams;
}

+ (MUFacebook*)sharedInstance;

- (NSString*)accessToken;

// this methods use in AppDelegate application:openURL:sourceApplication:annotation:
- (BOOL)handleOpenURL:(NSURL *)url;

// configurations:
// override this method in subclasses or used MUFacebookConfiguretion.plist
- (NSArray*)permissionsList;

// login\logout
- (void)loginWithCallback:(MUFacebookCallback)aCallback;
- (void)logout;

// user profile
- (void)userProfileWithCallback:(MUFacebookCallback)aCallback;

// friends list
- (void)friendsListWithCallback:(MUFacebookCallback)aCallback;

// post my wall, invite to friends
- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(MUFacebookCallback)aCallback;
- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(MUFacebookCallback)aCallback showDialog:(BOOL)aShowDialog;
- (void)sendInviteToFriendWithData:(NSDictionary*)aPostData callback:(MUFacebookCallback)aCallback;

@end
