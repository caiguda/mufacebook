//
//  MUFacebookUser.h
//  APITest
//
//  Created by Yuriy Bosov on 2/28/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MUFacebookUser : NSObject

+ (MUFacebookUser*)userWithDictinary:(NSDictionary*)aDictionary;

- (void)setupWithDictinary:(NSDictionary*)aDictionary;

@property (nonatomic, strong) NSString* ID;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* gender;
@property (nonatomic, strong) NSDate* birthday;

@property (nonatomic, strong) NSString* userLocationName;
@property (nonatomic, strong) NSURL* avatarURL;

@end
