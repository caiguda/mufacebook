//
//  MUFacebook.m
//  APITest
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "MUFacebook.h"
#import "FacebookSDK.h"

// Graph Path
#define kGraphPathMe                @"me"
#define kGraphPathMeFeed            @"me/feed"
#define kGraphPathMyFriends         @"me/friends"
#define kGraphPathApprequests       @"apprequests"

@interface MUFacebook()

- (void)applicationDidBecomeActive:(NSNotification*)aNotification;
- (void)setup;

@end


@implementation MUFacebook

+ (MUFacebook*)sharedInstance
{
    static MUFacebook* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      sharedInstance = [[self class] new];
                  });
    return sharedInstance;
}

- (NSString*)accessToken
{
    return [FBSession.activeSession accessTokenData].accessToken;
}

#pragma mark - Init\Settup
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setup];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    return self;
}

- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"MUFacebookConfiguretion" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSAssert(config, @"Need config file!");
    NSLog(@"facebook config %@", config);
    
    permissions = [config objectForKey:@"permissions"];
    if (!permissions)
        permissions = [self permissionsList];
    NSParameterAssert(permissions);

    profileParams = [config objectForKey:@"profileParams"];
}

#pragma mark - Configurations
- (NSArray*)permissionsList
{
    // override this method in subclasses or used MUFacebookConfiguretion.plist
    return [NSArray arrayWithObjects:@"user_about_me",@"user_birthday",@"email", @"user_photos", @"read_friendlists", nil];
}

// this methods use in AppDelegate application:openURL:sourceApplication:annotation:
- (BOOL)handleOpenURL:(NSURL*)url
{
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationDidBecomeActive:(NSNotification*)aNotification
{
    [FBSession.activeSession handleDidBecomeActive];
}

-(void)fbResync
{
    ACAccountStore *accountStore;
    ACAccountType *accountTypeFB;
    if ((accountStore = [[ACAccountStore alloc] init]) && (accountTypeFB = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook] ) ){
        
        NSArray *fbAccounts = [accountStore accountsWithAccountType:accountTypeFB];
        id account;
        if (fbAccounts && [fbAccounts count] > 0 && (account = [fbAccounts objectAtIndex:0])){
            
            [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                //we don't actually need to inspect renewResult or error.
                if (error)
                {
                    
                }
            }];
        }
    }
}

#pragma mark - Login, Logout, UserData
- (void)loginWithCallback:(MUFacebookCallback)aCallback
{
    NSParameterAssert(aCallback);
    [FBSession openActiveSessionWithReadPermissions:[self permissionsList]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
    {
        
        if(error)
        {
            [self fbResync];
            [NSThread sleepForTimeInterval:1];   //half a second
            [FBSession openActiveSessionWithReadPermissions:permissions
                                               allowLoginUI:YES
                                          completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
            {
                switch (status)
                {
                    case FBSessionStateOpen:
                        NSLog(@"accessToken %@", FBSession.activeSession.accessTokenData.accessToken);
                        NSLog(@"expirationDate %@", FBSession.activeSession.accessTokenData.expirationDate);
                        aCallback(FBSession.activeSession.accessTokenData.accessToken, nil, NO);
                        break;
                        
                    case FBSessionStateClosed:
                        aCallback(nil, error, YES);
                        NSLog(@"FBSessionStateClosed");
                        break;
                    case FBSessionStateClosedLoginFailed:
                        aCallback(nil, error, YES);
                        [FBSession.activeSession closeAndClearTokenInformation];
                        NSLog(@"need reiterate login");
                        break;
                        
                    default:
                        break;
                }
            }];
            
        }
        else
        {
            switch (status)
            {
                case FBSessionStateOpen:
                    NSLog(@"accessToken %@", FBSession.activeSession.accessTokenData.accessToken);
                    NSLog(@"expirationDate %@", FBSession.activeSession.accessTokenData.expirationDate);
                    aCallback(FBSession.activeSession.accessTokenData.accessToken, nil, NO);
                    break;
                                
                case FBSessionStateClosed:
                    aCallback(nil, nil, YES);
                    NSLog(@"FBSessionStateClosed");
                    break;
                case FBSessionStateClosedLoginFailed:
                    aCallback(nil, nil, YES);
                    [FBSession.activeSession closeAndClearTokenInformation];
                    NSLog(@"need reiterate login");
                    break;
                    
                default:
                    break;
            }
        }
    }];
}

- (void)logout
{
    NSLog(@"logout");
    [FBSession.activeSession close];
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession setActiveSession:nil];
}

- (void)userProfileWithCallback:(MUFacebookCallback)aCallback
{
    NSParameterAssert(aCallback);

    void(^block)(void) = ^(void)
    {
        if ([profileParams length])
        {
            NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:profileParams, @"fields",nil];
            FBRequest* request = [[FBRequest alloc] initWithSession:[FBSession activeSession] graphPath:kGraphPathMe parameters:params HTTPMethod:@"GET"];
            [request startWithCompletionHandler:^(FBRequestConnection *connection, FBGraphObject* result, NSError *error)
             {
                 NSLog(@"userProfileWithCallback");
                 NSLog(@"params %@", params);
                 NSLog(@"result %@", result);
                 NSLog(@"error %@", error);
                 if (result)
                 {
                     MUFacebookUser* user = [[MUFacebookUser alloc] init];
                     [user setupWithDictinary:result];
                     aCallback(user, nil, NO);
                 }
                 else
                 {
                     aCallback(nil, error, NO);
#warning TODO : Add correct error handler
                 }
             }];
        }
        else
        {
            [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
             {
                 NSLog(@"userProfileWithCallback");
                 NSLog(@"result %@", result);
                 NSLog(@"error %@", error);
                 
                 if (result)
                 {
                     MUFacebookUser* user = [[MUFacebookUser alloc] init];
                     [user setupWithDictinary:result];
                     aCallback(user, nil, NO);
                 }
                 else
                 {
                     aCallback(nil, error, NO);
#warning TODO : Add correct error handler
                 }
             }];
        }
    };

    if ([FBSession.activeSession isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
        {
            if (data)
            {
                block();
            }
            else
            {
                aCallback(data, error, canceled);
            }
        }];
    }
}

#pragma mark - Friends List
- (void)friendsListWithCallback:(MUFacebookCallback)aCallback
{
    NSParameterAssert(aCallback);
    
    void(^block)(void) = ^(void)
    {
        [[FBRequest requestForMyFriends] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
        {
#warning TODO
            NSLog(@"friendsListWithCallback");
            NSLog(@"result %@", result);
            NSLog(@"error %@", error);
            
            aCallback(result, error, NO);
        }];
    };
    
    if ([FBSession.activeSession isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }
}

#pragma mark - Post On My Wall
- (void)postOnMyWallWithData:(NSDictionary *)aPostData callback:(MUFacebookCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);

    void(^block)(void) = ^(void)
    {
        [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:[FBGraphObject graphObjectWrappingDictionary:aPostData]
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
        {
#warning TODO
            NSLog(@"postOnMyWallWithData");
            NSLog(@"FBWebDialogResult %d", result);
            NSLog(@"resultURL %@", resultURL);
            NSLog(@"error %@", error);
            
            aCallback(nil, nil, NO);
        }];
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }
}

- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(MUFacebookCallback)aCallback showDialog:(BOOL)aShowDialog
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
    
    void(^block)(void) = ^(void)
    {
        if (aShowDialog)
        {
            [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:[FBGraphObject graphObjectWrappingDictionary:aPostData]
                                                      handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
             {
#warning TODO
                 NSLog(@"postOnMyWallWithData show dialog");
                 NSLog(@"FBWebDialogResult %d", result);
                 NSLog(@"resultURL %@", resultURL);
                 NSLog(@"error %@", error);
                 
                 aCallback(nil, nil, NO);
             }];
        }
        else
        {
            
            void(^requestBlock)(void) = ^(void)
            {
                [FBRequestConnection startWithGraphPath:kGraphPathMeFeed parameters:[FBGraphObject graphObjectWrappingDictionary:aPostData] HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                 {
#warning TODO
                     NSLog(@"postOnMyWallWithData without dialog");
                     NSLog(@"FBWebDialogResult %@", result);
                     NSLog(@"error %@", error);
                     
                     aCallback(nil, nil, NO);
                 }];
            };
            
            if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
            {
                [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                                      defaultAudience:FBSessionDefaultAudienceFriends
                                                    completionHandler:^(FBSession *session, NSError *error)
                {
                     if (!error)
                     {
                         requestBlock();
                     }
                    else
                    {
                        aCallback(nil, error, NO);
                    }
                 }];
            }
            else
            {
                requestBlock();
            }
        }
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }
}

#pragma mark - Send App Invite
- (void)sendInviteToFriendWithData:(NSDictionary *)aPostData callback:(MUFacebookCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
    
    void(^block)(void) = ^(void)
    {
        [FBWebDialogs presentDialogModallyWithSession:[FBSession activeSession]
                                               dialog:kGraphPathApprequests
                                           parameters:aPostData
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
        {
#warning TODO
            NSLog(@"sendInviteToFriendWithData");
            NSLog(@"FBWebDialogResult %d", result);
            NSLog(@"resultURL %@", resultURL);
            NSLog(@"error %@", error);
            
            aCallback(nil, nil, NO);
        }];
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }

}

@end