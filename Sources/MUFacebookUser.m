//
//  MUFacebookUser.m
//  APITest
//
//  Created by Yuriy Bosov on 2/28/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "MUFacebookUser.h"
#import "MUKit.h"

@implementation MUFacebookUser

+ (MUFacebookUser*)userWithDictinary:(NSDictionary*)aDictionary
{
    MUFacebookUser* user = [[MUFacebookUser alloc] init];
    [user setupWithDictinary:aDictionary];
    return user;
}

- (void)setupWithDictinary:(NSDictionary*)aDictionary
{
    self.ID = MU_NULL_PROTECT([aDictionary objectForKey:@"id"]);
    self.email = MU_NULL_PROTECT([aDictionary objectForKey:@"email"]);
    self.firstName = MU_NULL_PROTECT([aDictionary objectForKey:@"first_name"]);
    self.lastName = MU_NULL_PROTECT([aDictionary objectForKey:@"last_name"]);
    self.name = MU_NULL_PROTECT([aDictionary objectForKey:@"name"]);
    self.username = MU_NULL_PROTECT([aDictionary objectForKey:@"username"]);
    
    self.userLocationName = MU_NULL_PROTECT([aDictionary valueForKeyPath:@"location.name"]);
    
    NSString* avatarStr = MU_NULL_PROTECT([aDictionary valueForKeyPath:@"picture.data.url"]);
    if ([avatarStr length])
    {
        self.avatarURL = [NSURL URLWithString:avatarStr];
    }
    
    NSString* db = MU_NULL_PROTECT([aDictionary objectForKey:@"birthday"]);
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"MM/dd/yyyy";
        self.birthday = [formatter dateFromString:db];
    }
}

@end
