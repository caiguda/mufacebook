//
//  MUAppDelegate.h
//  MUFacebook
//
//  Created by Yuriy Bosov on 3/20/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
