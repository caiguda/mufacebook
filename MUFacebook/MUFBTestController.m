//
//  MUFBTestController.m
//  MUFacebook
//
//  Created by Yuriy Bosov on 3/20/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "MUFBTestController.h"
#import "MUFacebook.h"

@interface MUFBTestController ()

@end

@implementation MUFBTestController

- (id)init
{
    self = [super initWithNibName:@"MUFBTestController" bundle:nil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

#pragma mark - Button Acttion
- (IBAction)loginButtonPressed:(id)sender
{
    [[MUFacebook sharedInstance] loginWithCallback:^(id data, NSError *error, BOOL canceled)
    {
        ;
    }];
}

- (IBAction)userProfileButtonPressed:(id)sender
{
    [[MUFacebook sharedInstance] userProfileWithCallback:^(id data, NSError *error, BOOL canceled)
     {
     }];
}

- (IBAction)logoutButtonPressed:(id)sender
{
    [[MUFacebook sharedInstance] logout];
}

- (IBAction)postOnMyWallButtonPressed:(id)sender
{
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
#warning : params - hardcode
    [params setObject:@"http://caiguda.com" forKey:kFeedLinkKey];
    
    [[MUFacebook sharedInstance] postOnMyWallWithData:params callback:^(id data, NSError *error, BOOL canceled)
     {
     }];
}

- (IBAction)postOnMyWallButtonPressedTEST:(id)sender
{
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
#warning : params - hardcode
    [params setObject:@"http://caiguda.com" forKey:kFeedLinkKey];
    
    [[MUFacebook sharedInstance] postOnMyWallWithData:params callback:^(id data, NSError *error, BOOL canceled)
     {
     } showDialog:NO];
}

- (IBAction)sendInviteButtonPressed:(id)sender
{
#warning : user id - hardcode
#warning : params - hardcode
    NSMutableDictionary* params = [NSMutableDictionary dictionary];
    [params setObject:@"Hey, I started using GuideSetter for the guides search in other countries and earning money. Join me! You need just to register, http://GuideSetter.com" forKey:kInviteMessageKey];
    [params setObject:@"Guidesetter" forKey:kInviteTitleKey];
    [params setObject:@"100004502887996" forKey:kInviteToKey];
    
    [[MUFacebook sharedInstance] sendInviteToFriendWithData:params callback:^(id data, NSError *error, BOOL canceled)
     {
//         NSLog(@"data %@", data);
//         NSLog(@"error %@", error);
     }];
}

- (IBAction)friendsListButtonPressed:(id)sender
{
    [[MUFacebook sharedInstance] friendsListWithCallback:^(id data, NSError *error, BOOL canceled)
     {
//         NSLog(@"data %@", data);
//         NSLog(@"error %@", error);
     }];
}

@end
