//
//  MUAppDelegate.m
//  MUFacebook
//
//  Created by Yuriy Bosov on 3/20/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "MUAppDelegate.h"
#import "MUFacebook.h"
#import "MUFBTestController.h"


@implementation MUAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController =  [MUFBTestController new];
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[MUFacebook sharedInstance] handleOpenURL:url];
    return YES;
}

@end
