//
//  main.m
//  MUFacebook
//
//  Created by Yuriy Bosov on 3/20/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MUAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MUAppDelegate class]));
    }
}
