//
//  MUFBTestController.h
//  MUFacebook
//
//  Created by Yuriy Bosov on 3/20/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MUFBTestController : UIViewController

- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)userProfileButtonPressed:(id)sender;
- (IBAction)logoutButtonPressed:(id)sender;
- (IBAction)postOnMyWallButtonPressed:(id)sender;
- (IBAction)sendInviteButtonPressed:(id)sender;
- (IBAction)friendsListButtonPressed:(id)sender;

@end
